const nodeMailer = require("nodemailer");

const defaultEmailData = { from: "noreply@node-react.com" };
const dotenv = require("dotenv");
dotenv.config();

exports.sendEmail = emailData => {
    const transporter = nodeMailer.createTransport({
        service: 'gmail',
        auth: {
            user: process.env.EMAIL_ADDRESS,
            pass: process.env.EMAIL_PASSWORD
        }
    });
    return (
        transporter
            .sendMail(emailData)
            .then(info => console.log(`Message sent: ${info.response}`))
            .catch(err => console.log(`Problem sending email: ${err}`))
    );
};